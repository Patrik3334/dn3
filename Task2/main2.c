/*
Napišite igro na podlagi igre Hangman (vislice). V igri vislice dobi igralec besedo, ki jo mora uganiti,
pri čemer je vsaka neznana črka besede predstavljena s podčrtajem. Igralec poskuša uganiti črko
besede tako, da jo vnese v terminal. Če je črka pravilna, se izpolni prazen prostor, ki ustreza tej
črki. Če črka ni pravilna, se nariše del slikice igralca. Igralec ima omejeno število nepravilnih
poskusov, preden je slika igralca na vislicah v celoti izrisana in je igra izgubljena. Igralec zmaga v igri,
če ugane vse črke besede, preden mu zmanjka poskusov.
Program mora imeti niz možnih besed, med katerimi lahko izbira. Vsakič, ko zaženemo program, je
treba iz matrike naključno izbrati drugo besedo. Igra mora prav tako izpisovati vse črke, ki jih je
igralec poizkusil.
Naj bo vaš program modularen (uporabljajte funkcije). V komentarjih navedite vse uporabljene
lekcije in koncepte ter njihov namen
*/
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h> //omogoca metodo "strlen" --> %zu, izracuna length nekega stringa!

void konecigre();
//void kazen();
void checkcrke();
char checkRekurzija();
void checkcrke2();
void checkcrke3();
void checkcrke4();

char option1[20] = "peleponez";
char option2[20] = "praktikum";
char option3[20] = "programiranje";
char option4[20] = "vislice";

int zivljene = 9;

char podcrtaj[20];

int main()
{
   printf("Enter the number of word for which you will guess letter that the word contains: \n");

   printf("1\n");
   printf("2\n");
   printf("3\n");
   printf("4\n");

   checkRekurzija();
}
char checkRekurzija()
{
  
   char check1;
   scanf("%c", &check1);
   if (check1 == '1')
   {
      printf("Length of word 1 = %zu \n", strlen(option1)); // izpise dolzino moje izbire

      for (int i = 0; i < strlen(option1); i++) // gre cez for loop in vsak characer zamenja z "_"
      {
         podcrtaj[i] = '_';
      }
      printf("%s", podcrtaj);

      checkcrke();

      
   }
   else if (check1 == '2')
   {
      printf("Length of word 2 = %zu \n", strlen(option2));

      for (int i = 0; i < strlen(option2); i++)
      {
         podcrtaj[i] = '_';
      }
      printf("%s", podcrtaj);

      checkcrke2();
   }
   else if (check1 == '3')
   {

      printf("Length of word 3 = %zu \n", strlen(option3));
      for (int i = 0; i < strlen(option3); i++)
      {
         podcrtaj[i] = '_';
      }
      printf("%s", podcrtaj);

      checkcrke3();
   }
   else if (check1 == '4')
   {
      printf("Length of word 4 = %zu \n", strlen(option4));

      for (int i = 0; i < strlen(option4); i++)
      {
         podcrtaj[i] = '_';
      }
      printf("%s", podcrtaj);
      
      checkcrke4();
   }
   else
   {

      checkRekurzija();
   }
}
void checkcrke()
{
   printf("\nEnter the character you think the word contains it:\n ");
    char input[20];
   scanf("%s", input);
   if ((!input[1]) && (input[0] > 60) && (input[0] < 123)) // preveri ali je v scanner dana samo ena crka
   {
      int count = 0;
      for (int i = 0; i < strlen(option1); i++)
      {
         if (input[0] == option1[i])
         {
            podcrtaj[i] = option1[i];
            count++;
         }
      }
      if (count == 0)
      {
         zivljene--;
         printf("Lives left: %i\n", zivljene);
         count = 0;
      }

      printf("%s\n", podcrtaj);
   }
  // kazen();
   konecigre();
   checkcrke();
   
}
void checkcrke2(){
    printf("\nEnter the character you think the word contains it:\n ");
    char input[20];
   scanf("%s", input);
   if ((!input[1]) && (input[0] > 60) && (input[0] < 123)) // preveri ali je v scanner dana samo ena crka
   {
      int count = 0;
      for (int i = 0; i < strlen(option2); i++)
      {
         if (input[0] == option2[i])
         {
            podcrtaj[i] = option2[i];
            count++;
         }
      }
      if (count == 0)
      {
         zivljene--;
         printf("Lives left: %i\n", zivljene);
         count = 0;
      }

      printf("%s", podcrtaj);
   }
  // kazen();
   konecigre();
   checkcrke2();
   

}
void checkcrke3(){
    printf("\nEnter the character you think the word contains it:\n ");
    char input[20];
   scanf("%s", input);
   if ((!input[1]) && (input[0] > 60) && (input[0] < 123)) // preveri ali je v scanner dana samo ena crka
   {
      int count = 0;
      for (int i = 0; i < strlen(option3); i++)
      {
         if (input[0] == option3[i])
         {
            podcrtaj[i] = option3[i];
            count++;
         }
      }
      if (count == 0)
      {
         zivljene--;
         printf("Lives left: %i\n", zivljene);
         count = 0;
      }

      printf("%s", podcrtaj);
   }
  // kazen();
   konecigre(); 
   checkcrke3();
  
}
void checkcrke4(){
    printf("\nEnter the character you think the word contains it:\n ");
    char input[20];
   scanf("%s", input);
   if ((!input[1]) && (input[0] > 60) && (input[0] < 123)) // preveri ali je v scanner dana samo ena crka
   {
      int count = 0;
      for (int i = 0; i < strlen(option4); i++)
      {
         if (input[0] == option4[i])
         {
            podcrtaj[i] = option4[i];
            count++;
         }
      }
      if (count == 0)
      {
         zivljene--;
         printf("Lives left: %i\n", zivljene);
         count = 0;
      }

      printf("%s", podcrtaj);
   }
  // kazen();
   konecigre();
   checkcrke4();
   

}
void konecigre(){
    bool nisi_koncal = true;
   for (int i = 0; i < strlen(podcrtaj); i++)
   {
      if (podcrtaj[i] == '_')
      {
         nisi_koncal = false;
      }
      
   }
   printf("\n");
   printf("\nIf the number at the end is 0 you havent finished the game, else you have!!! %d\n", nisi_koncal );
   if (nisi_koncal == true)
   {
      printf("\n");
      printf("\n");
      printf("YOU'VE WON THE GAME!!!!");
      printf("\n");
      printf("\n");
      exit(0);
   } 
   if (zivljene == 0)
   {
      printf("\n");
      printf("\n");
      printf("YOU'VE LOST ALL OF UR LIVES, BETTER LUCK NEXT TIME!!");
      printf("\n");
      printf("\n");
      exit(0);
   }
   

}
/*void kazen(){
if(zivljene == 9){
   
   printf("|                     |\n");
   printf("|                     |\n");
   printf("|                     |\n");
   printf("|                     |\n");
   printf("|                     |\n");
   printf("|                     |\n");
   printf("|                     |\n");
   printf("|   o                 |\n");
   printf("| \\|/                |\n");
   printf("|   |                 |\n");
   printf("|   /\\                |\n");

*/
/*}else if(zivljene = 8){

   printf("|         ||U ||      |\n");
   printf("|         || S||      |\n");
   printf("|         ||A ||      |\n");
   printf("|           \\_/       |\n");
   printf("|                     |\n");
   printf("|                     |\n");
   printf("|                     |\n");
   printf("|   o                 |\n");
   printf("|  \\|/                |\n");
   printf("|   |                 |\n");
   printf("|  /\\                |\n");
   
*/
/*}else if(zivljene == 7){
   printf("|                     |\n");
   printf("|         ||U ||      |\n");
   printf("|         || S||      |\n");
   printf("|         ||A ||      |\n");
   printf("|          \\_/       |\n");
   printf("|                     |\n");
   printf("|                     |\n");
   printf("|   o                 |\n");
   printf("| \\|/                |\n");
   printf("|   |                 |\n");
   printf("|  /\\                |\n");
   

*/
/*}else if(zivljene == 6){
   printf("|                     |\n");
   printf("|                     |\n");
   printf("|         ||U ||      |\n");
   printf("|         || S||      |\n");
   printf("|         ||A ||      |\n");
   printf("|          \\_/       |\n");
   printf("|                     |\n");
   printf("|   o                 |\n");
   printf("| \\|/                |\n");
   printf("|   |                 |\n");
   printf("|  /\\                |\n");
*/
/*}else if(zivljene == 5){
   printf("|                     |\n");
   printf("|                     |\n");
   printf("|                     |\n");
   printf("|         ||U ||      |\n");
   printf("|         || S||      |\n");
   printf("|         ||A ||      |\n");
   printf("|          \\_/       |\n");
   printf("|   o                 |\n");
   printf("| \\|/                |\n");
   printf("|   |                 |\n");
   printf("|  /\\                |\n");
*/
/*}else if(zivljene == 4){
   printf("|                     |\n");
   printf("|                     |\n");
   printf("|                     |\n");
   printf("|                     |\n");
   printf("|         ||U ||      |\n");
   printf("|         || S||      |\n");
   printf("|         ||A ||      |\n");
   printf("|   o      \\_/       |\n");
   printf("| \\|/                |\n");
   printf("|   |                 |\n");
   printf("|  /\\                |\n");
*/
/*}else if(zivljene == 3){
   printf("|                     |\n");
   printf("|                     |\n");
   printf("|                     |\n");
   printf("|                     |\n");
   printf("|                     |\n");
   printf("|         ||U ||      |\n");
   printf("|         || S||      |\n");
   printf("|    o    ||A ||      |\n");
   printf("|  \\|/    \\_/       |\n");
   printf("|    |                |\n");
   printf("|   /\\               |\n");
   

*/
/*}else if(zivljene == 2){
   printf("|                     |\n");
   printf("|                     |\n");
   printf("|                     |\n");
   printf("|                     |\n");
   printf("|                     |\n");
   printf("|                     |\n");
   printf("|         ||U ||      |\n");
   printf("|    o    || S||      |\n");
   printf("|  \\|/   ||A ||      |\n");
   printf("|    |     \\_/       |\n");
   printf("|   /\\               |\n");

*/
/*}else if(zivljene == 1){
   printf("|                     |\n");
   printf("|                     |\n");
   printf("|                     |\n");
   printf("|                     |\n");
   printf("|                     |\n");
   printf("|                     |\n");
   printf("|                     |\n");
   printf("|    o    ||U ||      |\n");
   printf("| \\ |/   || S||      |\n");
   printf("|    |    ||A ||      |\n");
   printf("|   /\\    \\_/       |\n");
   
*/
/*}else if(zivljene == 0){
   printf("|               o        |\n");
   printf("|        |   \\/ /       |\n");
   printf("|       /\\              |\n");
   printf("|                        |\n");
   printf("|     \\                 |\n");
   printf("|  \\    //    //        |\n");
   printf("|    \\    \\     //     |\n");
   printf("| \\ \\__//   \\\\       |\n");
   printf("| \\  /   \\\\       //  |\n");
   printf("|  \\/     \\  //    /// |\n");
   printf("|   /      \\     //     |\n");
   
}


}
*/
